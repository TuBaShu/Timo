package com.linln.admin.maintainLog.repository;

import com.linln.admin.maintainLog.domain.MaintainLog;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
public interface MaintainLogRepository extends BaseRepository<MaintainLog, Long> {
}