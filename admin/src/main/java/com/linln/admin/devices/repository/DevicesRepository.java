package com.linln.admin.devices.repository;

import com.linln.admin.devices.domain.Devices;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author xsz
 * @date 2020/09/26
 */
public interface DevicesRepository extends BaseRepository<Devices, Long> {
}