package com.linln.admin.maintainImage.service.impl;

import com.linln.admin.maintainImage.domain.MaintainImage;
import com.linln.admin.maintainImage.repository.MaintainImageRepository;
import com.linln.admin.maintainImage.service.MaintainImageService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
@Service
public class MaintainImageServiceImpl implements MaintainImageService {

    @Autowired
    private MaintainImageRepository maintainImageRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public MaintainImage getById(Long id) {
        return maintainImageRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<MaintainImage> getPageList(Example<MaintainImage> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return maintainImageRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param maintainImage 实体对象
     */
    @Override
    public MaintainImage save(MaintainImage maintainImage) {
        return maintainImageRepository.save(maintainImage);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return maintainImageRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}