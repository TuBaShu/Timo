package com.linln.admin.tendImage.service.impl;

import com.linln.admin.tendImage.domain.TendImage;
import com.linln.admin.tendImage.repository.TendImageRepository;
import com.linln.admin.tendImage.service.TendImageService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
@Service
public class TendImageServiceImpl implements TendImageService {

    @Autowired
    private TendImageRepository tendImageRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public TendImage getById(Long id) {
        return tendImageRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<TendImage> getPageList(Example<TendImage> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return tendImageRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param tendImage 实体对象
     */
    @Override
    public TendImage save(TendImage tendImage) {
        return tendImageRepository.save(tendImage);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return tendImageRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}