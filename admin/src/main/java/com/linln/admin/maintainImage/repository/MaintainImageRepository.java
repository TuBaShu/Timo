package com.linln.admin.maintainImage.repository;

import com.linln.admin.maintainImage.domain.MaintainImage;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
public interface MaintainImageRepository extends BaseRepository<MaintainImage, Long> {
}