package com.linln.admin.devices.validator;

import lombok.Data;

import java.io.Serializable;
import javax.validation.constraints.NotEmpty;

/**
 * @author xsz
 * @date 2020/09/26
 */
@Data
public class DevicesValid implements Serializable {
    @NotEmpty(message = "项目名称不能为空")
    private String title;
}