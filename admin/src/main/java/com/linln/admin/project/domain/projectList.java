package com.linln.admin.project.domain;

import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.StatusUtil;
import lombok.Data;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author xsz
 * @date 2020/09/26
 */
@Data
@Entity
@Table(name="xsz_projectList")
@EntityListeners(AuditingEntityListener.class)
@Where(clause = StatusUtil.NOT_DELETE)
public class projectList implements Serializable {
    // 项目编号
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    // 项目名称
    private String title;
    // 项目地址
    private String adderss;
    // 设备数量
    private Long devices;
    // 施工单位
    private String construction;
    // 租赁单位
    private String lessor;
    // 维保等级
    private String level;
    // 项目状态
    private Byte status = StatusEnum.OK.getCode();
    // 创建时间
    @CreatedDate
    private Date createDate;
}