package com.linln.admin.tendImage.controller;

import com.linln.admin.tendImage.domain.TendImage;
import com.linln.admin.tendImage.service.TendImageService;
import com.linln.admin.tendImage.validator.TendImageValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
@Controller
@RequestMapping("/tendImage/tendImage")
public class TendImageController {

    @Autowired
    private TendImageService tendImageService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("tendImage:tendImage:index")
    public String index(Model model, TendImage tendImage) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching();

        // 获取数据列表
        Example<TendImage> example = Example.of(tendImage, matcher);
        Page<TendImage> list = tendImageService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/tendImage/tendImage/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("tendImage:tendImage:add")
    public String toAdd() {
        return "/tendImage/tendImage/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("tendImage:tendImage:edit")
    public String toEdit(@PathVariable("id") TendImage tendImage, Model model) {
        model.addAttribute("tendImage", tendImage);
        return "/tendImage/tendImage/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"tendImage:tendImage:add", "tendImage:tendImage:edit"})
    @ResponseBody
    public ResultVo save(@Validated TendImageValid valid, TendImage tendImage) {
        // 复制保留无需修改的数据
        if (tendImage.getId() != null) {
            TendImage beTendImage = tendImageService.getById(tendImage.getId());
            EntityBeanUtil.copyProperties(beTendImage, tendImage);
        }

        // 保存数据
        tendImageService.save(tendImage);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("tendImage:tendImage:detail")
    public String toDetail(@PathVariable("id") TendImage tendImage, Model model) {
        model.addAttribute("tendImage",tendImage);
        return "/tendImage/tendImage/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("tendImage:tendImage:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (tendImageService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}