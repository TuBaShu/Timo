package com.linln.admin.project.controller;

import com.linln.admin.project.domain.projectList;
import com.linln.admin.project.service.projectListService;
import com.linln.admin.project.validator.projectListValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xsz
 * @date 2020/09/26
 */
@Controller
@RequestMapping("/project/projectList")
public class projectListController {

    @Autowired
    private projectListService projectListService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("project:projectList:index")
    public String index(Model model, projectList projectList) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("title", match -> match.contains())
                .withMatcher("adderss", match -> match.contains())
                .withMatcher("lessor", match -> match.contains());

        // 获取数据列表
        Example<projectList> example = Example.of(projectList, matcher);
        Page<projectList> list = projectListService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/project/projectList/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("project:projectList:add")
    public String toAdd() {
        return "/project/projectList/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("project:projectList:edit")
    public String toEdit(@PathVariable("id") projectList projectList, Model model) {
        model.addAttribute("projectList", projectList);
        return "/project/projectList/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"project:projectList:add", "project:projectList:edit"})
    @ResponseBody
    public ResultVo save(@Validated projectListValid valid, projectList projectList) {
        // 复制保留无需修改的数据
        if (projectList.getId() != null) {
            projectList beprojectList = projectListService.getById(projectList.getId());
            EntityBeanUtil.copyProperties(beprojectList, projectList);
        }

        // 保存数据
        projectListService.save(projectList);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("project:projectList:detail")
    public String toDetail(@PathVariable("id") projectList projectList, Model model) {
        model.addAttribute("projectList",projectList);
        return "/project/projectList/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("project:projectList:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (projectListService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}