package com.linln.admin.maintainLog.controller;

import com.linln.admin.maintainLog.domain.MaintainLog;
import com.linln.admin.maintainLog.service.MaintainLogService;
import com.linln.admin.maintainLog.validator.MaintainLogValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
@Controller
@RequestMapping("/maintainLog/maintainLog")
public class MaintainLogController {

    @Autowired
    private MaintainLogService maintainLogService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("maintainLog:maintainLog:index")
    public String index(Model model, MaintainLog maintainLog) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("project", match -> match.contains());

        // 获取数据列表
        Example<MaintainLog> example = Example.of(maintainLog, matcher);
        Page<MaintainLog> list = maintainLogService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/maintainLog/maintainLog/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("maintainLog:maintainLog:add")
    public String toAdd() {
        return "/maintainLog/maintainLog/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("maintainLog:maintainLog:edit")
    public String toEdit(@PathVariable("id") MaintainLog maintainLog, Model model) {
        model.addAttribute("maintainLog", maintainLog);
        return "/maintainLog/maintainLog/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"maintainLog:maintainLog:add", "maintainLog:maintainLog:edit"})
    @ResponseBody
    public ResultVo save(@Validated MaintainLogValid valid, MaintainLog maintainLog) {
        // 复制保留无需修改的数据
        if (maintainLog.getId() != null) {
            MaintainLog beMaintainLog = maintainLogService.getById(maintainLog.getId());
            EntityBeanUtil.copyProperties(beMaintainLog, maintainLog);
        }

        // 保存数据
        maintainLogService.save(maintainLog);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("maintainLog:maintainLog:detail")
    public String toDetail(@PathVariable("id") MaintainLog maintainLog, Model model) {
        model.addAttribute("maintainLog",maintainLog);
        return "/maintainLog/maintainLog/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("maintainLog:maintainLog:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (maintainLogService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}