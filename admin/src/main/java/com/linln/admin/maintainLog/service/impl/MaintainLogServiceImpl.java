package com.linln.admin.maintainLog.service.impl;

import com.linln.admin.maintainLog.domain.MaintainLog;
import com.linln.admin.maintainLog.repository.MaintainLogRepository;
import com.linln.admin.maintainLog.service.MaintainLogService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
@Service
public class MaintainLogServiceImpl implements MaintainLogService {

    @Autowired
    private MaintainLogRepository maintainLogRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public MaintainLog getById(Long id) {
        return maintainLogRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<MaintainLog> getPageList(Example<MaintainLog> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return maintainLogRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param maintainLog 实体对象
     */
    @Override
    public MaintainLog save(MaintainLog maintainLog) {
        return maintainLogRepository.save(maintainLog);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return maintainLogRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}