package com.linln.admin.bugImage.repository;

import com.linln.admin.bugImage.domain.BugImage;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/10/03
 */
public interface BugImageRepository extends BaseRepository<BugImage, Long> {
}