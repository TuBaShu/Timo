package com.linln.admin.bugReport.service;

import com.linln.admin.bugReport.domain.bugReport;
import com.linln.common.enums.StatusEnum;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author xsz
 * @date 2020/09/27
 */
public interface bugReportService {

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    Page<bugReport> getPageList(Example<bugReport> example);

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    bugReport getById(Long id);

    /**
     * 保存数据
     * @param bugReport 实体对象
     */
    bugReport save(bugReport bugReport);

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Transactional
    Boolean updateStatus(StatusEnum statusEnum, List<Long> idList);
}