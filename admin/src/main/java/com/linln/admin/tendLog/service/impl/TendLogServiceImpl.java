package com.linln.admin.tendLog.service.impl;

import com.linln.admin.tendLog.domain.TendLog;
import com.linln.admin.tendLog.repository.TendLogRepository;
import com.linln.admin.tendLog.service.TendLogService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
@Service
public class TendLogServiceImpl implements TendLogService {

    @Autowired
    private TendLogRepository tendLogRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public TendLog getById(Long id) {
        return tendLogRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<TendLog> getPageList(Example<TendLog> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return tendLogRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param tendLog 实体对象
     */
    @Override
    public TendLog save(TendLog tendLog) {
        return tendLogRepository.save(tendLog);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return tendLogRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}