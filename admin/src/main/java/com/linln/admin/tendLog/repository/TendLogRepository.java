package com.linln.admin.tendLog.repository;

import com.linln.admin.tendLog.domain.TendLog;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
public interface TendLogRepository extends BaseRepository<TendLog, Long> {
}