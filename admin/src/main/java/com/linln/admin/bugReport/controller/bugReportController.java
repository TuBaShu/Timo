package com.linln.admin.bugReport.controller;

import com.linln.admin.bugReport.domain.bugReport;
import com.linln.admin.bugReport.service.bugReportService;
import com.linln.admin.bugReport.validator.bugReportValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xsz
 * @date 2020/09/27
 */
@Controller
@RequestMapping("/bugReport/bugReport")
public class bugReportController {

    @Autowired
    private bugReportService bugReportService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("bugReport:bugReport:index")
    public String index(Model model, bugReport bugReport) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("title", match -> match.contains())
                .withMatcher("project", match -> match.contains());

        // 获取数据列表
        Example<bugReport> example = Example.of(bugReport, matcher);
        Page<bugReport> list = bugReportService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/bugReport/bugReport/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("bugReport:bugReport:add")
    public String toAdd() {
        return "/bugReport/bugReport/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("bugReport:bugReport:edit")
    public String toEdit(@PathVariable("id") bugReport bugReport, Model model) {
        model.addAttribute("bugReport", bugReport);
        return "/bugReport/bugReport/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"bugReport:bugReport:add", "bugReport:bugReport:edit"})
    @ResponseBody
    public ResultVo save(@Validated bugReportValid valid, bugReport bugReport) {
        // 复制保留无需修改的数据
        if (bugReport.getId() != null) {
            bugReport bebugReport = bugReportService.getById(bugReport.getId());
            EntityBeanUtil.copyProperties(bebugReport, bugReport);
        }

        // 保存数据
        bugReportService.save(bugReport);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("bugReport:bugReport:detail")
    public String toDetail(@PathVariable("id") bugReport bugReport, Model model) {
        model.addAttribute("bugReport",bugReport);
        return "/bugReport/bugReport/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("bugReport:bugReport:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (bugReportService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}