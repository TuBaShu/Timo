package com.linln.admin.contacts.controller;

import com.linln.admin.contacts.domain.Contacts;
import com.linln.admin.contacts.service.ContactsService;
import com.linln.admin.contacts.validator.ContactsValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xsz
 * @date 2020/09/26
 */
@Controller
@RequestMapping("/contacts/contacts")
public class ContactsController {

    @Autowired
    private ContactsService contactsService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("contacts:contacts:index")
    public String index(Model model, Contacts contacts) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("title", match -> match.contains());

        // 获取数据列表
        Example<Contacts> example = Example.of(contacts, matcher);
        Page<Contacts> list = contactsService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/contacts/contacts/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("contacts:contacts:add")
    public String toAdd() {
        return "/contacts/contacts/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("contacts:contacts:edit")
    public String toEdit(@PathVariable("id") Contacts contacts, Model model) {
        model.addAttribute("contacts", contacts);
        return "/contacts/contacts/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"contacts:contacts:add", "contacts:contacts:edit"})
    @ResponseBody
    public ResultVo save(@Validated ContactsValid valid, Contacts contacts) {
        // 复制保留无需修改的数据
        if (contacts.getId() != null) {
            Contacts beContacts = contactsService.getById(contacts.getId());
            EntityBeanUtil.copyProperties(beContacts, contacts);
        }

        // 保存数据
        contactsService.save(contacts);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("contacts:contacts:detail")
    public String toDetail(@PathVariable("id") Contacts contacts, Model model) {
        model.addAttribute("contacts",contacts);
        return "/contacts/contacts/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("contacts:contacts:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (contactsService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}