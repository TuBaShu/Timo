package com.linln.admin.contacts.validator;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xsz
 * @date 2020/09/26
 */
@Data
public class ContactsValid implements Serializable {
}