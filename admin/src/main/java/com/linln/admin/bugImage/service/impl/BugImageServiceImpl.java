package com.linln.admin.bugImage.service.impl;

import com.linln.admin.bugImage.domain.BugImage;
import com.linln.admin.bugImage.repository.BugImageRepository;
import com.linln.admin.bugImage.service.BugImageService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/03
 */
@Service
public class BugImageServiceImpl implements BugImageService {

    @Autowired
    private BugImageRepository bugImageRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public BugImage getById(Long id) {
        return bugImageRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<BugImage> getPageList(Example<BugImage> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return bugImageRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param bugImage 实体对象
     */
    @Override
    public BugImage save(BugImage bugImage) {
        return bugImageRepository.save(bugImage);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return bugImageRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}