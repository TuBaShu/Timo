package com.linln.admin.project.service.impl;

import com.linln.admin.project.domain.projectList;
import com.linln.admin.project.repository.projectListRepository;
import com.linln.admin.project.service.projectListService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author xsz
 * @date 2020/09/26
 */
@Service
public class projectListServiceImpl implements projectListService {

    @Autowired
    private projectListRepository projectListRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public projectList getById(Long id) {
        return projectListRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<projectList> getPageList(Example<projectList> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return projectListRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param projectList 实体对象
     */
    @Override
    public projectList save(projectList projectList) {
        return projectListRepository.save(projectList);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return projectListRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}