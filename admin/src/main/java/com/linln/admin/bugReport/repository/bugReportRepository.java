package com.linln.admin.bugReport.repository;

import com.linln.admin.bugReport.domain.bugReport;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author xsz
 * @date 2020/09/27
 */
public interface bugReportRepository extends BaseRepository<bugReport, Long> {
}