package com.linln.admin.tendImage.repository;

import com.linln.admin.tendImage.domain.TendImage;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
public interface TendImageRepository extends BaseRepository<TendImage, Long> {
}