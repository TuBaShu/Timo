package com.linln.admin.tendLog.controller;

import com.linln.admin.tendLog.domain.TendLog;
import com.linln.admin.tendLog.service.TendLogService;
import com.linln.admin.tendLog.validator.TendLogValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/04
 */
@Controller
@RequestMapping("/tendLog/tendLog")
public class TendLogController {

    @Autowired
    private TendLogService tendLogService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("tendLog:tendLog:index")
    public String index(Model model, TendLog tendLog) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("project", match -> match.contains());

        // 获取数据列表
        Example<TendLog> example = Example.of(tendLog, matcher);
        Page<TendLog> list = tendLogService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/tendLog/tendLog/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("tendLog:tendLog:add")
    public String toAdd() {
        return "/tendLog/tendLog/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("tendLog:tendLog:edit")
    public String toEdit(@PathVariable("id") TendLog tendLog, Model model) {
        model.addAttribute("tendLog", tendLog);
        return "/tendLog/tendLog/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"tendLog:tendLog:add", "tendLog:tendLog:edit"})
    @ResponseBody
    public ResultVo save(@Validated TendLogValid valid, TendLog tendLog) {
        // 复制保留无需修改的数据
        if (tendLog.getId() != null) {
            TendLog beTendLog = tendLogService.getById(tendLog.getId());
            EntityBeanUtil.copyProperties(beTendLog, tendLog);
        }

        // 保存数据
        tendLogService.save(tendLog);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("tendLog:tendLog:detail")
    public String toDetail(@PathVariable("id") TendLog tendLog, Model model) {
        model.addAttribute("tendLog",tendLog);
        return "/tendLog/tendLog/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("tendLog:tendLog:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (tendLogService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}