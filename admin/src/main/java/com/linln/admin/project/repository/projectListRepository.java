package com.linln.admin.project.repository;

import com.linln.admin.project.domain.projectList;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author xsz
 * @date 2020/09/26
 */
public interface projectListRepository extends BaseRepository<projectList, Long> {
}