package com.linln.admin.bugReport.service.impl;

import com.linln.admin.bugReport.domain.bugReport;
import com.linln.admin.bugReport.repository.bugReportRepository;
import com.linln.admin.bugReport.service.bugReportService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author xsz
 * @date 2020/09/27
 */
@Service
public class bugReportServiceImpl implements bugReportService {

    @Autowired
    private bugReportRepository bugReportRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public bugReport getById(Long id) {
        return bugReportRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<bugReport> getPageList(Example<bugReport> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return bugReportRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param bugReport 实体对象
     */
    @Override
    public bugReport save(bugReport bugReport) {
        return bugReportRepository.save(bugReport);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return bugReportRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}