package com.linln.admin.bugImage.controller;

import com.linln.admin.bugImage.domain.BugImage;
import com.linln.admin.bugImage.service.BugImageService;
import com.linln.admin.bugImage.validator.BugImageValid;
import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.EntityBeanUtil;
import com.linln.common.utils.ResultVoUtil;
import com.linln.common.utils.StatusUtil;
import com.linln.common.vo.ResultVo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 小懒虫
 * @date 2020/10/03
 */
@Controller
@RequestMapping("/bugImage/bugImage")
public class BugImageController {

    @Autowired
    private BugImageService bugImageService;

    /**
     * 列表页面
     */
    @GetMapping("/index")
    @RequiresPermissions("bugImage:bugImage:index")
    public String index(Model model, BugImage bugImage) {

        // 创建匹配器，进行动态查询匹配
        ExampleMatcher matcher = ExampleMatcher.matching();

        // 获取数据列表
        Example<BugImage> example = Example.of(bugImage, matcher);
        Page<BugImage> list = bugImageService.getPageList(example);

        // 封装数据
        model.addAttribute("list", list.getContent());
        model.addAttribute("page", list);
        return "/bugImage/bugImage/index";
    }

    /**
     * 跳转到添加页面
     */
    @GetMapping("/add")
    @RequiresPermissions("bugImage:bugImage:add")
    public String toAdd() {
        return "/bugImage/bugImage/add";
    }

    /**
     * 跳转到编辑页面
     */
    @GetMapping("/edit/{id}")
    @RequiresPermissions("bugImage:bugImage:edit")
    public String toEdit(@PathVariable("id") BugImage bugImage, Model model) {
        model.addAttribute("bugImage", bugImage);
        return "/bugImage/bugImage/add";
    }

    /**
     * 保存添加/修改的数据
     * @param valid 验证对象
     */
    @PostMapping("/save")
    @RequiresPermissions({"bugImage:bugImage:add", "bugImage:bugImage:edit"})
    @ResponseBody
    public ResultVo save(@Validated BugImageValid valid, BugImage bugImage) {
        // 复制保留无需修改的数据
        if (bugImage.getId() != null) {
            BugImage beBugImage = bugImageService.getById(bugImage.getId());
            EntityBeanUtil.copyProperties(beBugImage, bugImage);
        }

        // 保存数据
        bugImageService.save(bugImage);
        return ResultVoUtil.SAVE_SUCCESS;
    }

    /**
     * 跳转到详细页面
     */
    @GetMapping("/detail/{id}")
    @RequiresPermissions("bugImage:bugImage:detail")
    public String toDetail(@PathVariable("id") BugImage bugImage, Model model) {
        model.addAttribute("bugImage",bugImage);
        return "/bugImage/bugImage/detail";
    }

    /**
     * 设置一条或者多条数据的状态
     */
    @RequestMapping("/status/{param}")
    @RequiresPermissions("bugImage:bugImage:status")
    @ResponseBody
    public ResultVo status(
            @PathVariable("param") String param,
            @RequestParam(value = "ids", required = false) List<Long> ids) {
        // 更新状态
        StatusEnum statusEnum = StatusUtil.getStatusEnum(param);
        if (bugImageService.updateStatus(statusEnum, ids)) {
            return ResultVoUtil.success(statusEnum.getMessage() + "成功");
        } else {
            return ResultVoUtil.error(statusEnum.getMessage() + "失败，请重新操作");
        }
    }
}