package com.linln.admin.contacts.repository;

import com.linln.admin.contacts.domain.Contacts;
import com.linln.modules.system.repository.BaseRepository;

/**
 * @author xsz
 * @date 2020/09/26
 */
public interface ContactsRepository extends BaseRepository<Contacts, Long> {
}