package com.linln.admin.contacts.service.impl;

import com.linln.admin.contacts.domain.Contacts;
import com.linln.admin.contacts.repository.ContactsRepository;
import com.linln.admin.contacts.service.ContactsService;
import com.linln.common.data.PageSort;
import com.linln.common.enums.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author xsz
 * @date 2020/09/26
 */
@Service
public class ContactsServiceImpl implements ContactsService {

    @Autowired
    private ContactsRepository contactsRepository;

    /**
     * 根据ID查询数据
     * @param id 主键ID
     */
    @Override
    @Transactional
    public Contacts getById(Long id) {
        return contactsRepository.findById(id).orElse(null);
    }

    /**
     * 获取分页列表数据
     * @param example 查询实例
     * @return 返回分页数据
     */
    @Override
    public Page<Contacts> getPageList(Example<Contacts> example) {
        // 创建分页对象
        PageRequest page = PageSort.pageRequest();
        return contactsRepository.findAll(example, page);
    }

    /**
     * 保存数据
     * @param contacts 实体对象
     */
    @Override
    public Contacts save(Contacts contacts) {
        return contactsRepository.save(contacts);
    }

    /**
     * 状态(启用，冻结，删除)/批量状态处理
     */
    @Override
    @Transactional
    public Boolean updateStatus(StatusEnum statusEnum, List<Long> idList) {
        return contactsRepository.updateStatus(statusEnum.getCode(), idList) > 0;
    }
}