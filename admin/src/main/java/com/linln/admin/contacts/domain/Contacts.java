package com.linln.admin.contacts.domain;

import com.linln.common.enums.StatusEnum;
import com.linln.common.utils.StatusUtil;
import lombok.Data;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author xsz
 * @date 2020/09/26
 */
@Data
@Entity
@Table(name="xsz_contacts")
@EntityListeners(AuditingEntityListener.class)
@Where(clause = StatusUtil.NOT_DELETE)
public class Contacts implements Serializable {
    // 数据编号
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;
    // 项目名称
    private String title;
    // 机长姓名
    private String jzname;
    // 机长电话
    private String jzphone;
    // 安全员姓名
    private String aqyname;
    // 安全员电话
    private String aqyphone;
    // 资料员姓名
    private String zlyname;
    // 资料员电话
    private String zlyphone;
    // 监理姓名
    private String jlname;
    // 监理电话
    private String jlphone;
    // 租赁方姓名
    private String zlfname;
    // 租赁方电话
    private String zlfphone;
    // 创建时间
    @CreatedDate
    private Date createDate;
    // 数据状态
    private Byte status = StatusEnum.OK.getCode();
}